﻿using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using Sesion2_Lab01.com.isil.shader.d3d;
using Sesion2_Lab01.com.isil.render.camera;

namespace Sesion2_Lab01
{
    public class RCWaySegment
    {
        

        public float X;
        public float Y;
        public float Z;
        public float Rotation_X;
        public float Rotation_Y;
        public float Rotation_Z;

        public Matrix Rotation;

        private NModel _segmentArt;
        private Shader3DProgram _shader;

        public Shader3DProgram Shader
        {
            get
            {
                return _shader;
            }

            set
            {
                _shader = value;
            }
        }

        public RCWaySegment(NModel Art, Shader3DProgram Shader)
        {
            this.Shader = Shader;
            _segmentArt = Art;

            _segmentArt.SetShader(this.Shader);
            Rotation = Matrix.Identity;

        }
        public void UpdateAndDraw(float dt, RenderCamera Camera)
        {
            Shader.X = X;
            Shader.Y = Y;
            Shader.Z = Z;

            Shader.RotationX = Rotation_X;
            Shader.RotationY = Rotation_Y;
            Shader.RotationZ = Rotation_Z;

            Shader.mRotation = Rotation;

            _segmentArt.Draw(Camera.transformed, (int)dt);
        }
        
    }
}
