﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX.Direct3D11;

using FOFramework;
using Sesion2_Lab01.com.isil.render.camera;

namespace Sesion2_Lab01
{
    public class RollerCoaster3DGame:Game
    {
        

        public RollerCoaster3DGame(Device Device):base(Device)
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            AddScene(new RCMainScene(this));

            SetCurrentScene(0);
        }

        public override void Update(float dt)
        {
            base.Update(dt);
        }

        
    }
}
