﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SharpDX;
using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using FOFramework;
using Sesion2_Lab01.com.isil.graphics;
using Sesion2_Lab01.com.isil.shader.d2d;
using FOFramework.Hud;
using Sesion2_Lab01.com.isil.shader.d3d;
using FORectangle = FOFramework.Math.Rectangle;

namespace Sesion2_Lab01
{
    public class RCMainScene:Scene
    {
        



        CustomBezier _wayBezier;
        RCWay _rollerCoasterWay;
        NSkyBox3D _SkyBox;
        RCWagon _player;

        RCWagonSegment _test;


        ShaderTextureProgram commonShader2D;

        RCAlienFactory _enemiesFactory;

        StaticModel _enemyTest;
        

        public RCMainScene(Game Game):base(Game)
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            commonShader2D = new ShaderTextureProgram(Game.Device);
            commonShader2D.Load("Content/Fx_TexturePrimitive.fx");

            Game.InitializeHUD();
            Button btn1 = new Button(HUD, "Content/hud/button1Up.png", commonShader2D, new Vector2(0, 0));
            btn1.SetOnClickDownArt("Content/hud/button1Down.png",commonShader2D);
            btn1.SetOnClickUp(delegate () { _player.CurrentT = 0; });
            HUD.AddHudObject(btn1);

            
            

            Shader3DProgram s = new Shader3DProgram(Game.Device);
            s.Load("Content/Fx_PrimitiveTexture3D.fx");
            
            _enemyTest = new StaticModel(NModel.Load("Content/rail/RailAlien/Alien.obj", Matrix.Identity, s), s);

            
            

            _wayBezier.SetOffset(Vector3.Zero);
            _wayBezier.SetScale(Vector3.One);

            _wayBezier.AddSegment(new Vector3[] { new Vector3(0,-0.484375f,-2.462891f),new Vector3(0, 4.061515f, -2.510894f),
                                                    new Vector3(0.07621802f, 5.90439f, 3.200279f),new Vector3(0,5.800315f, 5.624475f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(0,5.800315f, 5.624475f),new Vector3(-0.1541441f, 5.589832f, 10.52719f),
                                                    new Vector3(0.8758435f, 9.38762f,15.2004f),new Vector3(-0.1178427f, 12.9873f, 16.07785f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(-0.1178427f, 12.9873f, 16.07785f),new Vector3(-1.345873f, 17.43589f, 17.16225f),
                                                    new Vector3(-6.853406f, 16.35414f, 21.90204f),new Vector3(-8.926419f, 14.07733f, 25.04976f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-8.926419f, 14.07733f, 25.04976f),new Vector3(-10.42419f, 12.4323f, 27.32402f),
                                                    new Vector3(-11.44187f, 8.409705f, 25.22454f),new Vector3(-10.53711f, 6.558594f, 22.74219f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-10.53711f, 6.558594f, 22.74219f),new Vector3(-9.460173f, 4.355218f, 19.78744f),
                                                    new Vector3(-7.700464f, 7.381065f, 11.90144f),new Vector3(-7.060019f, 7.951318f, 10.8948f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-7.060019f, 7.951318f, 10.8948f),new Vector3(-5.498958f, 9.341285f, 8.441139f),
                                                    new Vector3(-4.879461f, 14.63886f, 8.638134f),new Vector3(-8.148315f, 16.97544f, 10.81257f) });

            _wayBezier.ResetLongitude();
            _wayBezier.UpdateLongitude();

            _player = new RCWagon(this, _wayBezier);

            Vector3 p = _wayBezier.GetPosition(0);

            _enemyTest.X = p.X;
            _enemyTest.Y = p.Y;
            _enemyTest.Z = p.Z;

            _SkyBox = new NSkyBox3D("Content/skybox/spSkybox.png", 4, 3, 50000);
            _SkyBox.X = 0;
            _SkyBox.Y = 0;
            _SkyBox.Z = 0;

            _rollerCoasterWay = new RCWay(this);
            _rollerCoasterWay.Initialize();

            _enemiesFactory = new RCAlienFactory();
            _enemiesFactory.SetAlienSpawnCondition(delegate (ref float ElapsedTime)
            {
                if(ElapsedTime >= 1000)
                {
                    System.Diagnostics.Debug.WriteLine("enemy spawn");
                    ElapsedTime = 0;
                    return true;
                }
                return false;
            });
            _enemiesFactory.SetAlienDeleteCondition(delegate (StaticModel AlienToDelete)
            {
                Vector3 pos = new Vector3(AlienToDelete.X, AlienToDelete.Y, AlienToDelete.Z);

                if(/*Vector3.Distance(_wayBezier.GetPosition(_player.CurrentT),pos)>9f || */pos.Y <= -5f)
                {
                    return true;
                }

                return false;
                
            });
        }

        public override void Update(float dt)
        {
            Vector3 playerCurrPos = _wayBezier.GetPosition(_player.CurrentT);
            Quaternion rot = _wayBezier.GetRotation(_player.CurrentT ) * Quaternion.RotationAxis(_wayBezier.GetAsideNormalV3(_player.CurrentT),37 * NativeApplication.Deg2Rad);

            Matrix r = Matrix.RotationQuaternion(rot);r.Transpose();

            
            Camera.Rotation = Quaternion.RotationMatrix(r);


            //Vector3 pos = _wayBezier.GetPosition(_player.CurrentT - .02f) - Vector3.Transform(_wayBezier.GetNormalV3(_player.CurrentT), Quaternion.RotationAxis(_wayBezier.GetAsideNormalV3(_player.CurrentT-.02f), 225 * NativeApplication.Deg2Rad)) * 5.2f;
            Vector3 pos = playerCurrPos + _wayBezier.GetNormalV3(_player.CurrentT) * 4.2f-_wayBezier.GetForwardNormal(_player.CurrentT)*3.0f;
            
            Camera.Position = pos;



            FORectangle re = new FORectangle();
            re.Position = new Vector2(playerCurrPos.X , playerCurrPos.Z + 4f);
            re.Width = re.Height = 4;


            _enemyTest.X = playerCurrPos.X;
            _enemyTest.Y = playerCurrPos.Y+1;
            _enemyTest.Z = playerCurrPos.Z;

            

            _enemiesFactory.Update(dt, re);

            _rollerCoasterWay.Update(dt);
            _player.Update(dt);


            
            

            base.Update(dt);
        }

        public override void Draw(float dt)
        {

           
            _SkyBox.UpdateDraw(Game.RenderCamera, (int)dt);
            
            _rollerCoasterWay.Draw(dt);
            _player.Draw(dt);
            


            _enemiesFactory.Draw(Game.RenderCamera, dt);

        }

        public override void OnKeyDown(int Keycode)
        {
            _player.Controller.OnKeyDown(Keycode);
        }

        public override void OnKeyUp(int Keycode)
        {
            _player.Controller.OnKeyUp(Keycode);

        }

        public override void Free()
        {
            base.Free();

            _rollerCoasterWay = null;
        }
    }
}
