﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using SharpDX;
using FOFramework;
using Sesion2_Lab01.com.isil.shader.d3d;
using FOFramework.Math;

namespace Sesion2_Lab01
{

    public class RCWay
    {
        private Scene _sceneParent;
        private CustomBezier _wayBezier;

        private List<RCWaySegment> _wayRails;

        private Vector3 _position;

        private float _numRails;

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        

        private Shader3DProgram _railShader
        {
            get
            {
                Shader3DProgram r = new Shader3DProgram(_sceneParent.Game.Device);
                r.Load("Content/Fx_PrimitiveTexture3D.fx");
                return r;
            }
        }
        

        public RCWay(Scene SceneParent)
        {
            _sceneParent = SceneParent;
        }

        public void Initialize()
        {
            _wayBezier.SetOffset(Vector3.Zero);
            _wayBezier.SetScale(Vector3.One);

            _wayBezier.AddSegment(new Vector3[] { new Vector3(0,-0.484375f,-2.462891f),new Vector3(0, 4.061515f, -2.510894f),
                                                    new Vector3(0.07621802f, 5.90439f, 3.200279f),new Vector3(0,5.800315f, 5.624475f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(0,5.800315f, 5.624475f),new Vector3(-0.1541441f, 5.589832f, 10.52719f),
                                                    new Vector3(0.8758435f, 9.38762f,15.2004f),new Vector3(-0.1178427f, 12.9873f, 16.07785f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(-0.1178427f, 12.9873f, 16.07785f),new Vector3(-1.345873f, 17.43589f, 17.16225f),
                                                    new Vector3(-6.853406f, 16.35414f, 21.90204f),new Vector3(-8.926419f, 14.07733f, 25.04976f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-8.926419f, 14.07733f, 25.04976f),new Vector3(-10.42419f, 12.4323f, 27.32402f),
                                                    new Vector3(-11.44187f, 8.409705f, 25.22454f),new Vector3(-10.53711f, 6.558594f, 22.74219f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-10.53711f, 6.558594f, 22.74219f),new Vector3(-9.460173f, 4.355218f, 19.78744f),
                                                    new Vector3(-7.700464f, 7.381065f, 11.90144f),new Vector3(-7.060019f, 7.951318f, 10.8948f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-7.060019f, 7.951318f, 10.8948f),new Vector3(-5.498958f, 9.341285f, 8.441139f),
                                                    new Vector3(-4.879461f, 14.63886f, 8.638134f),new Vector3(-8.148315f, 16.97544f, 10.81257f) });

            /*_wayBezier.AddSegment(new Vector3[] { Vector3.Zero,new Vector3(-0.01493335f, 0f, 16.7834f),new Vector3(0.01745772f, 4.524283f, 18.82718f),new Vector3(0.003401041f, 10.3487f, 24.01501f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(0.003401041f, 10.3487f, 24.01501f),new Vector3(0.0295198f, 13.32883f, 26.5898f),
                                                    new Vector3(0.03823495f, 17.47383f, 30.15674f),new Vector3(0.02296329f, 19.06284f, 35.75133f) });
            _wayBezier.AddSegment(new Vector3[] { new Vector3(0.02296329f, 19.06284f, 35.75133f),new Vector3(0.02296329f, 20.20675f, 42.15725f),
                                                    new Vector3(-11.04245f, 18.29689f, 50.78044f),new Vector3(-16.21629f, 11.21421f, 44.70131f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-16.21629f, 11.21421f, 44.70131f),new Vector3(-19.68002f, 7.188361f, 40.14434f),
                                                    new Vector3(-24.2373f, 8.420909f, 41.28328f),new Vector3(-22.13918f, 13.09832f, 40.10255f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-22.13918f, 13.09832f, 40.10255f),new Vector3(-19.82497f, 17.63147f, 38.6905f),
                                                    new Vector3(-14.69898f, 16.7058f, 38.53986f),new Vector3(-15.1785f, 13.21756f, 37.84798f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-15.1785f, 13.21756f, 37.84798f),new Vector3(-14.89597f, 10.92872f, 37.06427f),
                                                    new Vector3(-18.53354f, 6.64959f, 36.68913f),new Vector3(-21.63531f, 8.57479f, 35.52861f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-15.1785f, 13.21756f, 37.84798f),new Vector3(-14.89597f, 10.92872f, 37.06427f),
                                                    new Vector3(-18.53354f, 6.64959f, 36.68913f),new Vector3(-21.63531f, 8.57479f, 35.52861f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-21.63531f, 8.57479f, 35.52861f),new Vector3(-26.5292f, 12.00028f, 34.0116f),
                                                    new Vector3(-17.09779f, 20.61747f, 32.76117f),new Vector3(-14.89655f, 12.9831f, 31.08491f) });

            _wayBezier.AddSegment(new Vector3[] { new Vector3(-14.89655f, 12.9831f, 31.08491f),new Vector3(-13.831f, 9.587387f, 30.32583f),
                                                    new Vector3(-18.84959f, 8.031151f, 30.0695f),new Vector3(-19.27702f, 4.996336f, 21.75625f) });*/

            _wayBezier.ResetLongitude();
            _wayBezier.UpdateLongitude();

            _wayRails = new List<RCWaySegment>();
            Shader3DProgram newShader = new Shader3DProgram(_sceneParent.Game.Device);
            newShader.Load("Content/Fx_PrimitiveTexture3D.fx");
            NModel newRail = NModel.Load("Content/rail/RailSegment/Rail.obj", Matrix.Identity, newShader);

            for (float i = 0; i < 0.999f; i+= 1/_wayBezier.Longitude)
            {
                RCWaySegment newSeg = new RCWaySegment(newRail, newShader);

                Vector3 newRailPosition = _wayBezier.GetPosition(i);


                newSeg.X = newRailPosition.X;
                newSeg.Y = newRailPosition.Y;
                newSeg.Z = newRailPosition.Z;

                

                Matrix rot = Matrix.RotationQuaternion(_wayBezier.GetRotation(i));

                newSeg.Rotation = rot;

                

                

                _wayRails.Add(newSeg);

                
            }

            //MaterializeRail(Vector3.UnitX);
            
        }

        /*private void GenerateRail()
        {
            int railResolution = 8;
            List<Vector3> vertices = new List<Vector3>();
            List<uint> indices = new List<uint>();

            float _railSegLong = 0.50f;

            for (float i = 0; i < 0.99f - (_railSegLong / _wayBezier.Longitude); i+= _railSegLong / _wayBezier.Longitude)
            {
                Vector3 zPrime1 = _wayBezier.GetPosition(i + 0.0001f) - _wayBezier.GetPosition(i);
                zPrime1.Normalize();

                Vector3 zPrime2 = _wayBezier.GetPosition(i + _railSegLong / _wayBezier.Longitude + 0.0001f) - _wayBezier.GetPosition(i + _railSegLong / _wayBezier.Longitude);
                zPrime1.Normalize();

                Vector3 yPrime1 = GetRailNormal(i);

                Vector3[] circ1Points = FCircumference.GetPointsBasedOn(_wayBezier.GetPosition(i), .2f, railResolution, zPrime1);
                Vector3[] circ2Points = FCircumference.GetPointsBasedOn(_wayBezier.GetPosition(i + _railSegLong / _wayBezier.Longitude), .2f, railResolution, zPrime2);

                for (int j = 0; j < circ2Points.Length-1; j++)
                {
                    vertices.Add(circ2Points[j]);                    
                    vertices.Add(circ2Points[j+1]);
                    vertices.Add(circ1Points[j]);
                    vertices.Add(circ1Points[j+1]);

                    indices.Add((uint)(j * 4 ));
                    indices.Add((uint)(j * 4 + 1));
                    indices.Add((uint)(j * 4 + 2));
                    indices.Add((uint)(j * 4 + 3));
                }
            }

            _art = new Mesh(vertices.ToArray(), indices.ToArray());
        }*/

        /*private void MaterializeRail(Vector3 OffsetFromBezier)
        {
            List<Vector3> meshPoints = new List<Vector3>();
            List<uint> meshIndicies = new List<uint>();
            List<Vector2> uvs = new List<Vector2>();

            int meshResolution = 10;

            float iterations = 120;

            float radius = .1f;

            Vector3 off = Vector3.Transform(Vector3.UnitY, _wayBezier.GetAsideNormal(0));

            meshPoints.AddRange(Circ2D.CircDefinedBy(radius, _wayBezier.GetPosition(0) + off * OffsetFromBezier.X, _wayBezier.GetForwardNormal(0),
                _wayBezier.GetNormal(0), meshResolution));

            

            for (int k = 0; k < meshResolution; k++)
            {
                if (k == 0) uvs.Add(new Vector2(0, k));
                else uvs.Add(new Vector2(0, k + 1));
            }

            for (float i = (1 / iterations); i <= 0.999f; i += (1 / iterations))
            {


                int seg = _wayBezier.GetSegmentAt(i);

                Vector3 upN = Vector3.Transform( Vector3.UnitY, _wayBezier.GetRotation(i));

                Vector3 Center = _wayBezier.GetPosition(i) + off * OffsetFromBezier.X;
                //if (seg == 2) Center -= Bezier.CustomBezier.GetAsideNormal(i) * Vector3.up * OffsetFromBezier.x * 2;


                meshPoints.AddRange(Circ2D.CircDefinedBy(radius, Center, _wayBezier.GetForwardNormal(i), upN, meshResolution));

                int aux = (int)(i * iterations);

                for (int j = 0; j < meshResolution; j++)
                {
                    //meshIndicies.Add(aux * meshResolution + j);
                    //meshIndicies.Add((aux - 1) * meshResolution + j);
                    //meshIndicies.Add((aux - 1) * meshResolution + ((j == 0) ? meshResolution - 1 : j - 1));

                    //meshIndicies.Add(aux * meshResolution + j);
                    //meshIndicies.Add(aux * meshResolution + ((j == meshResolution - 1) ? 0 : j + 1));

                    //meshIndicies.Add((aux - 1) * meshResolution + j);

                    meshIndicies.Add((uint)((aux - 1) * meshResolution + j));
                    meshIndicies.Add((uint)(aux * meshResolution + j));
                    meshIndicies.Add((uint)((aux - 1) * meshResolution + ((j == 0) ? meshResolution - 1 : j - 1)));



                    meshIndicies.Add((uint)(aux * meshResolution + ((j == meshResolution - 1) ? 0 : j + 1)));
                    meshIndicies.Add((uint)(aux * meshResolution + j));
                    meshIndicies.Add((uint)((aux - 1) * meshResolution + j));


                    if (j == 0) uvs.Add(new Vector2(aux, j));
                    else uvs.Add(new Vector2(aux, j + 1));



                }
            }

            _art = new Mesh(meshPoints.ToArray(), meshIndicies.ToArray());
        }*/

        public void Update(float dt)
        {
            
        }

        public void Draw(float dt)
        {
            _wayRails.ForEach(r => r.UpdateAndDraw((int)dt, _sceneParent.Game.RenderCamera));
            //_art.Draw(_sceneParent.Game.RenderCamera, (int)dt);
        }

        private Vector3 GetRailNormal(float t)
        {
            Vector3 a = _wayBezier.GetPosition(t + 0.0001f) - _wayBezier.GetPosition(t);
            a.Normalize();

            System.Diagnostics.Debug.WriteLine(a);

            Vector3 b = Vector3.Cross(new Vector3(0, 1, 0), a);

            System.Diagnostics.Debug.WriteLine("b " +b);

            b = Vector3.Cross(b, a);

            b.Normalize();

            return b;
        }
    }
}
