﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;

using SharpDX;
using FOFramework;
using Sesion2_Lab01.com.isil.shader.d3d;

namespace Sesion2_Lab01
{
    public class RCWagon
    {
        

        private RCWagonSegment[] WagonSections;

        

        

        private Scene _parent;

        private CustomBezier _route;

        private float _t;

        private float _dt;

        private const float _segmentSeparation = .02f;

        private RCWagonController _controller;

        public float CurrentT
        {
            get { return _t; }
            set { _t = value; }
        }

        public float VelocityT
        {
            get
            {
                return _dt;
            }

            set
            {
                _dt = value;
            }
        }

        public RCWagonController Controller
        {
            get
            {
                return _controller;
            }            
        }

        public RCWagon(Scene SceneParent, CustomBezier Route)
        {
            _parent = SceneParent;
            Shader3DProgram _shader = new Shader3DProgram(_parent.Game.Device);
            _shader.Load("Content/Fx_PrimitiveTexture3D.fx");
            NModel _art = NModel.Load("Content/rail/RailWagon/Wagon.obj", Matrix.Identity, _shader);

            WagonSections = new RCWagonSegment[] { new RCWagonSegment(_art,_shader), new RCWagonSegment(_art, _shader), new RCWagonSegment(_art, _shader), new RCWagonSegment(_art, _shader), new RCWagonSegment(_art, _shader) };
            

            _route = Route;


            _controller = new RCWagonController(this);
        }

        public void Update(float dt)
        {
            _controller.Update(dt);
            if(_t < .999f - (_dt/100*dt))
            {
                //_t *= .99f;
                for (int i = 0; i < WagonSections.Length; i++)
                {
                    Vector3 newPosition = _route.GetPosition(_t - _segmentSeparation * i);

                    Quaternion newRot = _route.GetRotation(_t - _segmentSeparation * i) ;

                    newPosition += Vector3.Transform(Vector3.UnitY, newRot) * .5f;

                    WagonSections[i].X = newPosition.X;
                    WagonSections[i].Y = newPosition.Y;
                    WagonSections[i].Z = newPosition.Z;

                    WagonSections[i].Rotation = Matrix.RotationQuaternion(newRot);
                }
                if (_dt < 0) _dt = 0;
                if (_dt > .01f) _dt = .01f;

                _dt *= .99f;

                _t += _dt * (dt/100) ;

                

                
                
            }
        }

        public void Draw(float dt)
        {
            foreach (RCWagonSegment Wagon in WagonSections)
            {
                Wagon.UpdateDraw(_parent.Game.RenderCamera, (int)dt);
            }
        }
    }
}
