﻿using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using FOFramework;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.shader.d3d;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FORectangle = FOFramework.Math.Rectangle;

namespace Sesion2_Lab01
{
    public class RCAlienFactory
    {
        private List<StaticModel> _aliens;

        private NModel _commonAlienArt;

        private Shader3DProgram _commonAlienShader;

        public delegate bool AlienDeleteConditionDelegate(StaticModel AlienToDelete);

        public delegate bool AlienSpawnConditionDelegate(ref float ElapsedTime);

        private AlienSpawnConditionDelegate _alienSpawnCondition;
        private AlienDeleteConditionDelegate _alienDeleteCondition;

        private float _elapsedTime;

        private Random _rnd;

        public RCAlienFactory()
        {
            _commonAlienShader = new Shader3DProgram(NativeApplication.instance.Device);
            _commonAlienShader.Load("Content/Fx_PrimitiveTexture3D.fx");
            _commonAlienArt = NModel.Load("Content/rail/RailAlien/Alien.obj", Matrix.Identity, _commonAlienShader);
            _commonAlienArt.SetShader(_commonAlienShader);
            _aliens = new List<StaticModel>();
            _rnd = new Random();
            
        }

        public void SetAlienSpawnCondition(AlienSpawnConditionDelegate AlienSpawnCondition)
        {
            _alienSpawnCondition = AlienSpawnCondition;
        }

        public void SetAlienDeleteCondition(AlienDeleteConditionDelegate AlienDeleteCondition)
        {
            _alienDeleteCondition = AlienDeleteCondition;
        }

        public void Update(float dt, FORectangle SpawnBox)
        {
            _elapsedTime += dt;
            if(_alienSpawnCondition !=null)
            {
                if (_alienSpawnCondition(ref _elapsedTime)) SpawnAlien(SpawnBox);
            }

            if(_alienDeleteCondition != null && _aliens.Count > 0)
            {
                for (int i = 0; i < _aliens.Count; i++)
                {
                    if (_alienDeleteCondition(_aliens[i])) 
                    {
                        _aliens.RemoveAt(i);
                        i--;
                    }
                }
            }

            _aliens.ForEach(delegate(StaticModel a) {
                a.Y -= .7f;
                a.Rotation = Matrix.Identity;
                });
        }

        private void SpawnAlien(FORectangle SpawnBox)
        {
            StaticModel newAlien = new StaticModel(_commonAlienArt, _commonAlienShader);

            Vector3 pos = _rnd.NextVector3(new Vector3(SpawnBox.Min.X, 20f, SpawnBox.Min.Y), new Vector3(SpawnBox.Max.X, 20f, SpawnBox.Max.Y));

            newAlien.X = pos.X;
            newAlien.Y = pos.Y;
            newAlien.Z = pos.Z;

            

            _aliens.Add(newAlien);
        }

        private void DeleteAlien()
        {

        }

        public void Draw(RenderCamera Camera, float dt)
        {
            _aliens.ForEach(a => a.UpdateDraw(Camera, dt));
        }
    }
}
