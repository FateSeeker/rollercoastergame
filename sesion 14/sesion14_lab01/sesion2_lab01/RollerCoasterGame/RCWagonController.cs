﻿using Sesion2_Lab01.com.isil.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sesion2_Lab01
{
    public class RCWagonController
    {
        

        private const uint Forward =     0x00000001;
        private const uint Backward =    0x00000010;

        private uint _direction = 0;

        private RCWagon _target;

        private float _acc;
        private float _deacc;

        public RCWagonController(RCWagon Target)
        {
            _target = Target;
            _acc = .0001f;
            _deacc = .0001f;
        }

        public void OnKeyDown(int Keycode)
        {
            switch(Keycode)
            {
                case EnumNKeyCode.W:
                    _direction |= Forward;
                    
                    break;
                case EnumNKeyCode.S:
                    _direction |= Backward;
                    

                    break;
            }
        }
        public void OnKeyUp(int Keycode)
        {

            switch (Keycode)
            {
                case EnumNKeyCode.W:
                    
                    _direction ^= Forward;
                    
                    break;
                case EnumNKeyCode.S:
                    _direction ^= Backward;
                    

                    break;
            }
        }

        public void Update(float dt)
        {
            
            if ((_direction & Forward) == Forward) { _target.VelocityT += _acc ;  }
            if ((_direction & Backward) == Backward) { _target.VelocityT -= _deacc ; }

            
            
        }
    }
}
