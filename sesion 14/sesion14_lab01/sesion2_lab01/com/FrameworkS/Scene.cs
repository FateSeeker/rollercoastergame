﻿using RollerCoasterGame.com.FrameworkS.Core.HUD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollerCoasterGame.com.FrameworkS
{
    public class Scene
    {
        private Game _game;

        public Game Game { set { _game = value; } get { return _game; } }

        

        private RenderCamera _camera;

        public RenderCamera Camera { set { _camera = value; } get { return _camera; } }

        public HUD HUD { get { return Game.HUD; } }

        public Scene(Game Game)
        {
            _game = Game;
        }

        public virtual void Initialize()
        {
            _camera = new RenderCamera(new NViewport(NativeApplication.App_Width, NativeApplication.App_Height), -.5f);
            _camera.ChangeCameraTo(RenderCamera.PERSPECTIVE);
            Game.RenderCamera = _camera;
        }

        public virtual void Update(float dt)
        {
            _camera.Update();
        }

        public virtual void Draw(float dt)
        {
            
        }

        public virtual void OnKeyDown(int Keycode)
        {

        }

        public virtual void OnKeyUp(int Keycode)
        {

        }

        public virtual void OnMouseDown()
        {

        }

        public virtual void OnMouseUp()
        {

        }

        public virtual void Free()
        {
            _game = null;
        }
    }
}
