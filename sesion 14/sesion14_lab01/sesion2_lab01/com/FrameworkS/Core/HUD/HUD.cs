﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollerCoasterGame.com.FrameworkS.Core.HUD
{
    public class HUD
    {
        private Game _game;

        private List<HUDObject> _content;

        public HUD(Game Game)
        {
            _game = Game;
            
        }

        public void AddHudObject(HUDObject NewHudObject)
        {
            _content.Add(NewHudObject);
        }

        public void Initialize()
        {
            _content = new List<HUDObject>();
            //_content.ForEach(c => c.Initialize());
        }

        public void Update(float dt)
        {
            _content.ForEach(c => c.Update(dt));
        }

        public void Draw(float dt)
        {
            
            _content.ForEach(c => c.Draw(_game.RenderCamera, dt));
        }

        public bool HasContent() { return _content.Count > 0; }

        public void OnKeyDown(int Keycode)
        {
            _content.ForEach(c => c.OnKeyDown(Keycode));

        }

        public void OnKeyUp(int Keycode)
        {
            _content.ForEach(c => c.OnKeyUp(Keycode));

        }

        public void OnMouseDown()
        {
            _content.ForEach(c => c.OnMouseDown());
        }

        public void OnMouseUp()
        {
            _content.ForEach(c => c.OnMouseUp());
        }

        public void OnMouseHover(Vector2 MousePosition)
        {
            _content.ForEach(delegate(HUDObject C)
            {
                if (C.Rect.ContainsPoint(MousePosition)) C.OnMouseHover();
            });
        }

        public void Free()
        {
            _content.Clear();
            _content = null;
            _game = null;
        }
    }
}
