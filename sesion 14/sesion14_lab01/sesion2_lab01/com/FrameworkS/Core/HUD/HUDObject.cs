﻿
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FRectangle = RollerCoasterGame.com.FrameworkS.Core.Math.Rectangle;

namespace RollerCoasterGame.com.FrameworkS.Core.HUD
{
    public class HUDObject
    {
        protected HUD _hud;

        protected FRectangle _rect;

        public Vector2 Position
        {
            get { return _rect.Position; }
            set { _rect.Position = value; }
        }

        public FRectangle Rect
        {
            get { return _rect; }
        }
        
        public HUDObject(HUD HUD)
        {
            _hud = HUD;
        }

        

        public virtual void OnKeyDown(int Keycode)
        {

        }

        public virtual void OnKeyUp(int Keycode)
        {

        }

        public virtual void Update(float dt)
        {

        }

        public virtual void Draw(RenderCamera Camera, float dt)
        {

        }
        public virtual void OnMouseDown()
        {

        }
        public virtual void OnMouseUp()
        {

        }
        public virtual void OnMouseHover()
        {

        }
    }
}
