﻿using RollerCoasterGame.com.isil.graphics;
using RollerCoasterGame.com.isil.shader.d2d;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollerCoasterGame.com.FrameworkS.Core.HUD
{
    public class Button:HUDObject
    {
        private NSprite2D _artOnClickDown;
        private NSprite2D _artOnClickUp;
        private NSprite2D _artOnHover;

        private NSprite2D _currentArt;

        private Action _OnClickDown;

        private Action _OnClickUp;

        private Action _OnMouseHover;

        

        public Button(HUD Parent,string ArtPath,ShaderTextureProgram Shader ,Vector2 Position):base(Parent)
        {
            SetOnClickUpArt(ArtPath,Shader);
            _currentArt = _artOnClickUp;
            _rect.Position = Position;
        }

        public void Initialize()
        {            
            _rect.Width = _currentArt.Width;
            _rect.Height = _currentArt.Height;
        }

        public override void Update(float dt)
        {
            _currentArt.X = Position.X;
            _currentArt.Y = Position.Y;
        }

        public void SetOnClickDown(Action OnClickDownAction)
        {
            if (OnClickDownAction != null) _OnClickDown = OnClickDownAction;
        }

        public void SetOnClickUp(Action OnClickUpAction)
        {
            if (OnClickUpAction != null) _OnClickUp = OnClickUpAction;
        }

        public void SetOnMouseHover(Action OnMouseHoverAction)
        {
            if (OnMouseHoverAction != null) _OnMouseHover = OnMouseHoverAction;
        }

        public void SetOnClickDownArt(string Path, ShaderTextureProgram Shader)
        {
            _artOnClickDown = NSprite2D.Load(Path, Shader);
        }
        public void SetOnClickUpArt(string Path,ShaderTextureProgram Shader)
        {
            _artOnClickUp = NSprite2D.Load(Path, Shader);
        }
        public void SetOnHoverArt(string Path, ShaderTextureProgram Shader)
        {
            _artOnHover = NSprite2D.Load(Path, Shader);
        }

        public override void OnMouseDown()
        {
            if (_artOnClickDown != null) _currentArt = _artOnClickDown;
            if (_OnClickDown != null) _OnClickDown();
        }

        public override void OnMouseUp()
        {
            if (_artOnClickUp != null) _currentArt = _artOnClickUp;
            if (_OnClickUp != null) _OnClickUp();

        }

        public override void OnMouseHover()
        {
            if (_artOnHover != null) _currentArt = _artOnHover;
            if (_OnMouseHover != null) _OnMouseHover();

        }

        public override void Draw(RenderCamera Camera, float dt)
        {
            _currentArt.Draw(Camera, (int)dt);
        }

        
    }
}
