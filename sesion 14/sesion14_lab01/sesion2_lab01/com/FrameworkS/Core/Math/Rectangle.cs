﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollerCoasterGame.com.FrameworkS.Core.Math
{
    public struct Rectangle
    {
        private Vector2 _max;       

        private Vector2 _min;        

        public float Width
        {
            get
            {
                return _max.X - _min.X;
            }
            set
            {
                float crrW = (value - _max.X + _min.X);
                _max.X += crrW;                
            }
        }

        public float Height
        {
            get
            {
                return _max.Y - _min.Y;
            }
            set
            {
                float crrW = (value - _max.Y + _min.Y);
                _max.Y += crrW;
            }
        }

        public Vector2 Position
        {
            get { return _min; }
            set
            {
                _max += (value - _min);
                _min = value;                
            }
        }

        public bool ContainsPoint(Vector2 Point)
        {
            return ((Point.X <= _max.X && Point.X >= _min.X) && (Point.Y <= _max.Y && Point.Y >= _min.Y));
        }
    }
}
