﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;

using D3DBuffer = SharpDX.Direct3D11.Buffer;

namespace RollerCoasterGame.com.FrameworkS.Core.Graphics
{
    public class Mesh
    {
        private Vector3[] _vertices;
        private uint[] _indices;

        private Color4[] _colorPerVertex;

        private Shader3DSimpleProgram _shader;

        private Vector3[] _normals;

        private D3DBuffer _vertexBuffer;
        private D3DBuffer _indexBuffer;
        private VertexBufferBinding _vertexBufferBinding;

        private float[] _bufferVertices;

        

        #region PosRotnScale
        public float x { get { return _shader.X; }
            set { _shader.X = value; } }

        public float y
        {
            get { return _shader.Y; }
            set { _shader.Y = value; }
        }

        public float z
        {
            get { return _shader.Z; }
            set { _shader.Z = value; }
        }

        public float Rotation_X
        {
            get { return _shader.RotationX; }
            set { _shader.RotationX = value; }
        }

        public float Rotation_Y
        {
            get { return _shader.RotationY; }
            set { _shader.RotationY = value; }
        }

        public float Rotation_Z
        {
            get { return _shader.RotationZ; }
            set { _shader.RotationZ = value; }
        }

        public float Scale_X
        {
            get { return _shader.ScaleX; }
            set { _shader.ScaleX = value; }
        }

        public float Scale_Y
        {
            get { return _shader.ScaleY; }
            set { _shader.ScaleY = value; }
        }

        
        #endregion

        public Mesh(Vector3[] Vertices, uint[] Indices)
        {
            

            _shader = new Shader3DSimpleProgram(NativeApplication.instance.Device);

            _shader.Load("Content/Fx_PrimitiveSimple3D.fx");
            
            _indices = Indices;
            _vertices = Vertices;
            _normals = new Vector3[_vertices.Length];
            RecalculateNormals();
            SetColor(Color.Gray);

            _bufferVertices = new float[_vertices.Length * 5 + _colorPerVertex.Length * 4 + _normals.Length * 3  ];

            FillBufferVertices();

            DoBufferOperations();

        }

        public void SetColor(Color4 Color)
        {
            _colorPerVertex = new Color4[_vertices.Length];
            for (int i = 0; i < _colorPerVertex.Length; i++)
            {
                _colorPerVertex[i] = Color;
            }
        }

        public void RecalculateNormals()
        {
            for (int i = 0; i < _normals.Length/4; i++)
            {
                Vector3 a = _vertices[i * 4] + (_vertices[i * 4 + 1] - _vertices[i * 4]) * .5f; 
                Vector3 b = _vertices[i * 4 + 2] + (_vertices[i * 4 + 3] - _vertices[i * 4 + 2]) * .5f;
                Vector3 c = b + (a - b) * 0.5f;

                Vector3 normal = Vector3.Cross(c - a, c - b);

                _normals[i * 4] = normal;
                _normals[i * 4 + 1] = normal;
                _normals[i * 4 + 2] = normal;
                _normals[i * 4 + 3] = normal;
            }
        }

        private void FillBufferVertices()
        {
            for (int i = 0; i < _vertices.Length; i++)
            {
                _bufferVertices[i * 12] = _vertices[i].X;
                _bufferVertices[i * 12 + 1] = _vertices[i].Y;
                _bufferVertices[i * 12 + 2] = _vertices[i].Z;

                _bufferVertices[i * 12 + 3] = _colorPerVertex[i].Red;
                _bufferVertices[i * 12 + 4] = _colorPerVertex[i].Green;
                _bufferVertices[i * 12 + 5] = _colorPerVertex[i].Blue;
                _bufferVertices[i * 12 + 6] = _colorPerVertex[i].Alpha;

                _bufferVertices[i * 12 + 7] = _normals[i].X;
                _bufferVertices[i * 12 + 8] = _normals[i].Y;
                _bufferVertices[i * 12 + 9] = _normals[i].Z;

                _bufferVertices[i * 12 + 10] = 0;
                _bufferVertices[i * 12 + 11] = 0;

            }
        }

        private void DoBufferOperations()
        {
            if (_vertexBuffer != null) { _vertexBuffer.Dispose(); _vertexBuffer = null; }

            _vertexBuffer = D3DBuffer.Create<float>(NativeApplication.instance.Device, BindFlags.VertexBuffer, _bufferVertices);

            _vertexBufferBinding.Offset = 0;
            _vertexBufferBinding.Stride = sizeof(float) *  _bufferVertices.Length;
            _vertexBufferBinding.Buffer = _vertexBuffer;

            if (_indexBuffer != null) { _indexBuffer.Dispose();  _indexBuffer = null; }

            _indexBuffer = D3DBuffer.Create<uint>(NativeApplication.instance.Device, BindFlags.IndexBuffer, _indices);

        }

        public void Draw(RenderCamera Camera, float dt)
        {
            _shader.Update(_indexBuffer, _vertexBufferBinding);
            _shader.Draw(_indices.Length, Camera.transformed, SharpDX.Direct3D.PrimitiveTopology.TriangleList);
        }
    }
}
