﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using RollerCoasterGame.com.isil.utils;
using RollerCoasterGame.com.FrameworkS.Core.HUD;

namespace RollerCoasterGame.com.FrameworkS
{
    public class Game
    {
        private RenderCamera _camera;
        public RenderCamera RenderCamera { set { _camera = value; } get { return _camera; } }

        

        private Device _device;
        public Device Device { get { return _device; } }

        private NKeyboardHandler _keyboardHandler;
        private NMouseHandler _mouseHandler;

        public NKeyboardHandler Keyboard { get { return _keyboardHandler; } }
        public NMouseHandler Mouse { get { return _mouseHandler; } }

        public List<Scene> _scenes;

        private Scene _currentScene;

        private HUD _hud;

        public HUD HUD { set { _hud = value; } get { return _hud; } }

        public Scene CurrentScene
        {
            get { return _currentScene; } 
        }

        public Game(Device Device, RenderCamera Camera)
        {
            _device = Device;
            _camera = Camera;
        }

        public virtual void Initialize()
        {
            _keyboardHandler = new NKeyboardHandler(NativeApplication.instance.RenderForm, OnKeyDown, OnKeyUp);
            _mouseHandler = new NMouseHandler(NativeApplication.instance.RenderForm);

            _scenes = new List<Scene>();
        }

        public void AddScene(Scene NewScene)
        {
            _scenes.Add(NewScene);            
        }

        public void SetCurrentScene(int SceneIndex)
        {
            if(_currentScene != null)
            {
                _currentScene.Free();
                _currentScene = null;
            }
            
            _currentScene = _scenes[SceneIndex];
            _currentScene.Initialize();
        }

        public virtual void OnKeyDown(int Keycode)
        {
            _currentScene.OnKeyDown(Keycode);
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnKeyDown(Keycode);
            }
        }

        public virtual void OnKeyUp(int Keycode)
        {
            _currentScene.OnKeyUp(Keycode);
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnKeyUp(Keycode);
            }
        }

        public virtual void Update(float dt)
        {            
            Mouse.Update((int)dt);
            if(Mouse.TouchCollection.Count > 0 )
            {
                switch (Mouse.TouchCollection.Last().TouchState)
                {
                    case isil.data.NTouchLocationState.Pressed:
                        OnMouseDown();
                        break;
                    case isil.data.NTouchLocationState.Moved:
                        if(_hud !=null)
                        {
                            if (_hud.HasContent()) _hud.OnMouseHover(Mouse.TouchCollection.Last().Position);
                        }
                        break;
                    case isil.data.NTouchLocationState.Released:
                        OnMouseUp();
                        break;
                }

            }

            _currentScene.Update(dt);

            if(_hud!=null)
            {
                if (_hud.HasContent()) _hud.Update(dt);
            }
        }

        public void InitializeHUD()
        {

            _hud = new HUD(this);
            _hud.Initialize();
        }
        public void FreeHUD()
        {
            _hud.Free();
            _hud = null;
        }

        protected virtual void OnMouseDown()
        {
            _currentScene.OnMouseDown();
            if(_hud != null)
            {
                if (_hud.HasContent()) _hud.OnMouseDown();
            }
        }

        protected virtual void OnMouseUp()
        {
            _currentScene.OnMouseUp();
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnMouseUp();
            }
        }

        public virtual void Draw(float dt)
        {
            _currentScene.Draw(dt);
            if(_hud != null)
            {
                if(_hud.HasContent())
                {

                    _camera.ChangeCameraTo(RenderCamera.ORTHOGRAPHIC);

                    _hud.Draw(dt);

                    _camera.ChangeCameraTo(RenderCamera.PERSPECTIVE);

                }
            }
        }
    }
}
