﻿using SharpDX;

using System;
using System.Collections.Generic;




public struct CustomBezier {

    

    
    private List<Vector3> ControlPoints;    

    
    private int _numberOfSegments;

    
    private float _longitude;

    private Vector3 Offset;

    private Vector3 _scale;

    



    public int NumberOfSegments
    {
        get { return _numberOfSegments; }
    }

    public List<Vector3> Control_Points
    {
        get { return ControlPoints; }
    }

    public float Longitude
    {
        get { return _longitude; }
    }

    public Vector3 Scale
    {
        get { return _scale; }
    }

    

    public void SetOffset(Vector3 Offset)
    {
        this.Offset = Offset;
    }

    public void SetScale(Vector3 Scale)
    {
        this._scale = Scale;
    }

    

    public Vector3 GetPosition(float Parameter)
    {
        

        Vector3 result = Vector3.Zero;

        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ( (acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex)/_longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        

        result = GetSegmentPosition(realParameter, desiredIndex);

        

        return result + Offset;

    }

    
    public void VerifyData()
    {
        
    }

    public Quaternion GetRotation(float Parameter)
    {
        

        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        Vector3 fwd = (GetSegmentPosition(realParameter + 0.0002f, desiredIndex) - GetSegmentPosition(realParameter, desiredIndex));
        fwd.Normalize();

        Quaternion a = Quaternion.RotationLookAtRH(fwd, GetSegmentNormal(0, desiredIndex) * ((desiredIndex == 2) ? -1 : 1));

        Quaternion b = Quaternion.Identity;

        /*Matrix a = Matrix.LookAtRH(GetSegmentPosition(realParameter, desiredIndex), (GetSegmentPosition(realParameter + 0.0002f, desiredIndex)), 
            GetSegmentNormal(0, desiredIndex) * ((desiredIndex == 2) ? -1 : 1));
        Matrix b = Matrix.Identity;*/

        

        if (desiredIndex == _numberOfSegments)
        {
            //Vector3.Orthonormalize(orth2, fwd, GetSegmentNormal(0.999f, desiredIndex) * ((desiredIndex + 1 == 2) ? -1 : 1));

            b = Quaternion.RotationLookAtRH(fwd, GetSegmentNormal(0.999f, desiredIndex) * ((desiredIndex == 2) ? -1 : 1));
            /*b = Matrix.LookAtRH(GetSegmentPosition(realParameter, desiredIndex), (GetSegmentPosition(realParameter + 0.0002f, desiredIndex)),
            GetSegmentNormal(0.999f, desiredIndex) * ((desiredIndex == 2) ? -1 : 1));*/

        }
        else
        {
            //Vector3.Orthonormalize(orth2, fwd, GetSegmentNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2 || desiredIndex + 1 == 5) ? -1 : 1));

            /*b = Matrix.LookAtRH(GetSegmentPosition(realParameter, desiredIndex), (GetSegmentPosition(realParameter + 0.0002f, desiredIndex)),
            GetSegmentNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2) ? -1 : 1));*/

            b = Quaternion.RotationLookAtRH(fwd, GetSegmentNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2) ? -1 : 1));
        }

        
        return Quaternion.Lerp(a, b, realParameter);
    }

    public Vector3 GetNormalV3(float Parameter)
    {
        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        Vector3 a = GetSegmentNormal(0, desiredIndex) * ((desiredIndex == 2) ? -1 : 1);

        Vector3 b = Vector3.Zero;

        



        if (desiredIndex == _numberOfSegments)
        {
            

            b = GetSegmentNormal(0.999f, desiredIndex) * ((desiredIndex == 2) ? -1 : 1);
            

        }
        else
        {
            

            b = GetSegmentNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2) ? -1 : 1);
        }

        return Vector3.Lerp(a, b, realParameter);
    }

    public Quaternion GetAsideNormal(float Parameter)
    {
        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        Vector3 fwd = (GetSegmentPosition(realParameter + 0.0002f, desiredIndex) - GetSegmentPosition(realParameter, desiredIndex));
        fwd.Normalize();

        Quaternion a = Quaternion.RotationLookAtRH(fwd,
                GetSegmentAsideNormal(0, desiredIndex) * ((desiredIndex == 2) ? -1 : 1));

        Quaternion b = Quaternion.Identity;

        if (desiredIndex == _numberOfSegments)
        {

            b = Quaternion.RotationLookAtLH(fwd,
                GetSegmentAsideNormal(0.999f, desiredIndex) * ((desiredIndex + 1 == 2) ? -1 : 1));

        }
        else
        {


            b = Quaternion.RotationLookAtLH(fwd,
                    GetSegmentAsideNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2) ? -1 : 1));
        }


        return Quaternion.Lerp(a, b, realParameter);
    }

    public Vector3 GetAsideNormalV3(float Parameter)
    {
        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }



        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        Vector3 a = GetSegmentAsideNormal(0, desiredIndex) * ((desiredIndex == 2) ? -1 : 1);

        Vector3 b = Vector3.Zero;

        if (desiredIndex == _numberOfSegments)
        {

            b = GetSegmentAsideNormal(0.999f, desiredIndex) * ((desiredIndex + 1 == 2) ? -1 : 1);

        }
        else
        {


            b = GetSegmentAsideNormal(0, desiredIndex + 1) * ((desiredIndex + 1 == 2) ? -1 : 1);
        }

        return Vector3.Lerp(a,b,realParameter);
    }

    public Vector3 GetSegmentAsideNormal(float Parameter, int Segment)
    {
        Vector3[] SegmentPoints = new Vector3[] { ControlPoints[Segment * 4 - 4], ControlPoints[Segment * 4 - 3], ControlPoints[Segment * 4 - 2], ControlPoints[Segment * 4 - 1] };

        Vector3 a = BasicMovementFunctions.QuadBezier(SegmentPoints[1], SegmentPoints[2], SegmentPoints[3], Parameter) - GetSegmentPosition(Parameter, Segment);
        a.Normalize();
        Vector3 b = BasicMovementFunctions.QuadBezier(SegmentPoints[1], SegmentPoints[2], SegmentPoints[3], Parameter + 0.0005f) - GetSegmentPosition(Parameter + 0.0005f, Segment);
        b.Normalize();

        Vector3 result = Vector3.Cross(b, a);

        result.Normalize();


        return result;
    }

    public Vector3 GetForwardNormal(float Parameter)
    {
        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        Vector3 result = (GetSegmentPosition(realParameter + 0.0002f, desiredIndex) - GetSegmentPosition(realParameter, desiredIndex));
        result.Normalize();

        return result;

    }

    


    public Vector3 GetRawPosition(float Parameter)
    {
        int desiredSegment = (int)Math.Ceiling( Parameter * _numberOfSegments);
        if (desiredSegment < 1) desiredSegment = 1;

        
        float realParameter = Parameter*_numberOfSegments - desiredSegment + 1;



        return GetSegmentPosition(realParameter, desiredSegment);
    }

    

    public int GetCurrentSegment(float Parameter)
    {

        float acumLong = 0;

        int result = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                result = i;
                break;
            }
        }

        return result;
    }

    public void ResetLongitude()
    {
        _longitude = 0;
    }

    private Vector3 GetSegmentPosition(float Parameter, int Segment)
    {
        Vector3[] SegmentPoints = new Vector3[] { ControlPoints[Segment * 4 - 4], ControlPoints[Segment * 4 - 3], ControlPoints[Segment * 4 - 2], ControlPoints[Segment * 4 - 1] };

        SegmentPoints[0].X *= _scale.X;
        SegmentPoints[0].Y *= _scale.Y;
        SegmentPoints[0].Z *= _scale.Z;

        SegmentPoints[1].X *= _scale.X;
        SegmentPoints[1].Y *= _scale.Y;
        SegmentPoints[1].Z *= _scale.Z;

        SegmentPoints[2].X *= _scale.X;
        SegmentPoints[2].Y *= _scale.Y;
        SegmentPoints[2].Z *= _scale.Z;

        SegmentPoints[3].X *= _scale.X;
        SegmentPoints[3].Y *= _scale.Y;
        SegmentPoints[3].Z *= _scale.Z;

        return BasicMovementFunctions.Bezier(SegmentPoints[0], SegmentPoints[3], SegmentPoints[1], SegmentPoints[2],Parameter);
    }

    public Vector3 GetRawNormal(float Parameter)
    {
        int desiredSegment = (int)Math.Ceiling(Parameter * _numberOfSegments);
        if (desiredSegment < 1) desiredSegment = 1;


        float realParameter = Parameter * _numberOfSegments - desiredSegment + 1;

        return GetSegmentNormal(realParameter, desiredSegment);
    }

    public Vector3 GetNormal(float Parameter)
    {
        Vector3 result = Vector3.Zero;

        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        return GetSegmentNormal(realParameter, desiredIndex);
    }

    public Vector3 GetSegmentNormal(float Parameter, int Segment)
    {
        Vector3[] SegmentPoints = new Vector3[] { ControlPoints[Segment * 4 - 4], ControlPoints[Segment * 4 - 3], ControlPoints[Segment * 4 - 2], ControlPoints[Segment * 4 - 1] };

        Vector3 a = BasicMovementFunctions.QuadBezier(SegmentPoints[1], SegmentPoints[2], SegmentPoints[3], Parameter) - GetSegmentPosition(Parameter,Segment);
        a.Normalize();
        Vector3 b = BasicMovementFunctions.QuadBezier(SegmentPoints[1], SegmentPoints[2], SegmentPoints[3], Parameter + 0.0005f) - GetSegmentPosition(Parameter + 0.0005f, Segment );
        b.Normalize();

        Vector3 result = Vector3.Cross(Vector3.Cross(b, a), a);

        result.Normalize();


        return result;
    }

    public void AddSegment(Vector3[] NewControlPoints)
    {
        if (ControlPoints == null) { ControlPoints = new List<Vector3>();  }

        ControlPoints.AddRange(NewControlPoints);
        _numberOfSegments++;

        
    }

    public int GetSegmentAt(float Parameter)
    {
        float acumLong = 0;

        int desiredIndex = 0;

        for (int i = 1; i <= _numberOfSegments; i++)
        {
            acumLong += (GetSegmentLongitude(i) / _longitude);

            if ((acumLong) >= Parameter)
            {
                desiredIndex = i;
                break;
            }
        }

        //float realParameter = (Parameter - (acumLong - GetSegmentLongitude(desiredIndex) / _longitude)) / (GetSegmentLongitude(desiredIndex) / _longitude);

        return desiredIndex;
    }
    

    public void UpdateLongitude()
    {
        _longitude = 0;
        for (int i = 1; i <= _numberOfSegments; i++)
        {
            _longitude += GetSegmentLongitude(i);
        }
        
    }

    public void RemoveSegment(int Segment)
    {
        _longitude -= GetSegmentLongitude(Segment);
        _numberOfSegments--;
        ControlPoints.RemoveRange((Segment - 1)*4, 4);
        
        
    }

    private float GetSegmentLongitude(Vector3[] SegmentPoints)
    {
        float result = 0;
        for (float i = 0; i < 1; i+=.1f)
        {
            result += Vector3.Distance(BasicMovementFunctions.Bezier(SegmentPoints[0], SegmentPoints[3], SegmentPoints[1], SegmentPoints[2], i),
                BasicMovementFunctions.Bezier(SegmentPoints[0], SegmentPoints[3], SegmentPoints[1], SegmentPoints[2], i + .1f));
        }
        return result;
    }

    private float GetSegmentLongitude(int Segment)
    {
        float result = 0;

        Vector3[] SegmentPoints = new Vector3[] { ControlPoints[Segment * 4 - 4], ControlPoints[Segment * 4 - 3], ControlPoints[Segment * 4 - 2], ControlPoints[Segment * 4 - 1] };

        for (float i = 0; i < 1; i += .1f)
        {
            result += Vector3.Distance(BasicMovementFunctions.Bezier(SegmentPoints[0], SegmentPoints[3], SegmentPoints[1], SegmentPoints[2], i),
                BasicMovementFunctions.Bezier(SegmentPoints[0], SegmentPoints[3], SegmentPoints[1], SegmentPoints[2], i + .1f));
        }
        return result;
    }

    public void AlignStartAndEnds(bool IncludeFirstAndLastPoints)
    {
        if(_numberOfSegments > 1)
        {
            for (int i = 1; i < _numberOfSegments; i++)
            {
                ControlPoints[(i - 1) * 4 + 3] = ControlPoints[i * 4];
            }
            
        }
        if (IncludeFirstAndLastPoints) ControlPoints[0] = ControlPoints[ControlPoints.Count - 1];
    }

    

}
