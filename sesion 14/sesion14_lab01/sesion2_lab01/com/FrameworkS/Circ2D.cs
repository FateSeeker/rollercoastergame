﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace RollerCoasterGame.com.FrameworkS
{
    public struct Circ2D
    {
        public static List<Vector3> CircDefinedBy(float Radius, Vector3 Center, Vector3 ForwardNormal, Vector3 UpNormal, int Resolution)
        {
            List<Vector3> result = new List<Vector3>();

            float angleIteration = (float)Math.PI * 2 / Resolution;

            for (int i = 0; i < Resolution; i++)
            {
                Vector3 n = Vector3.Cross(ForwardNormal, UpNormal) * Radius + Center;
                Quaternion rot = Quaternion.RotationAxis(ForwardNormal, angleIteration * (float)(180 / Math.PI) * i);
                n = Vector3.Transform(n, rot);
                //n = Vector3.Transform(n, Matrix.RotationAxis(ForwardNormal, angleIteration * (float)(180 / Math.PI) * i));
                result.Add(n);
            }

            return result;
        }
    }
}

