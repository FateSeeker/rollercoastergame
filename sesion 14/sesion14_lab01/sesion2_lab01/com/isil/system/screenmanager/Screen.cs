﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01.com.isil.system.screenManager {
    public class Screen {

        public Screen() {

        }

        public virtual void Initialize() {

        }

        public virtual void OnKeyDown(int keyCode)
        {

        }

        public virtual void OnKeyUp(int keyCode)
        {

        }

        public virtual void UpdateDraw(int dt) {

        }

        public virtual void Destroy() {

        }
    }
}
