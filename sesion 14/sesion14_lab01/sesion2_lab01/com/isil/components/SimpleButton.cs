﻿using Sesion2_Lab01.com.isil.data;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.render.graphics;
using Sesion2_Lab01.com.isil.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01.com.isil.components {
    public class SimpleButton {

        private Sprite2D mSprite;
        private Action mClickCallback;

        public float X
        {
            get { return mSprite.X; }
            set { mSprite.X = value; }
        }

        public float Y
        {
            get { return mSprite.Y; }
            set { mSprite.Y = value; }
        }

        public float ScaleX
        {
            get { return mSprite.ScaleX; }
            set { mSprite.ScaleX = value; }
        }

        public float ScaleY
        {
            get { return mSprite.ScaleY; }
            set { mSprite.ScaleY = value; }
        }

        public float Width
        {
            get { return mSprite.Width; }
        }

        public float Height
        {
            get { return mSprite.Height; }
        }

        public SimpleButton(int x, int y, string texturePath) {

            mSprite = new Sprite2D(texturePath);
            this.X = x;
            this.Y = y;
        }

        public void OnClickCallback(Action callbackClick)
        {
            mClickCallback = callbackClick;
        }

        public void UpdateDraw(RenderCamera camera, int dt)
        {
            /*NMouseHandler mouse = NativeApplication.instance.MouseHandler;

            for (int i = 0; i < mouse.TouchCollection.Count; i++)
            {
                NTouchState state = mouse.TouchCollection[i];

                if (mSprite.HitTest((int)state.Position.X, (int)state.Position.Y))
                {
                    if (mClickCallback != null)
                    {
                        mClickCallback();
                    }
                }
            }

            mSprite.UpdateDraw(dt, camera);*/
        }
    }
}
