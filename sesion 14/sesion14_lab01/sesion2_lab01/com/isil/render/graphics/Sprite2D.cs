﻿using Sesion2_Lab01.com.isil.content;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.shader.d2d;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01
{
    public class Sprite2D
    {
        private NTexture2D mTexture2D;
        private ShaderTextureProgram mShaderProgram;

        // variables para dibujar nuestro primitivo
        private ushort[] mIndices;
        private float[] mVertices;

        public float X
        {
            get { return mShaderProgram.X; }
            set { mShaderProgram.X = value; }
        }

        public float Y
        {
            get { return mShaderProgram.Y; }
            set { mShaderProgram.Y = value; }
        }

        public float ScaleX
        {
            get { return mShaderProgram.ScaleX; }
            set { mShaderProgram.ScaleX = value; }
        }

        public float ScaleY
        {
            get { return mShaderProgram.ScaleY; }
            set { mShaderProgram.ScaleY = value; }
        }

        public float Width
        {
            get { return mTexture2D.Width; }
        }

        public float Height
        {
            get { return mTexture2D.Height; }
        }



        public Sprite2D(string path , ShaderTextureProgram Shader)
        {
            // cargamos nuestra textura
            mTexture2D = new NTexture2D(NativeApplication.instance.Device);
            mTexture2D.Load(path);

            // cargamos y construimos nuestro Shader
            /*mShaderProgram = new ShaderTextureProgram(NativeApplication.instance.Device);
            mShaderProgram.Load("Content/Fx_TexturePrimitive.fx");*/

            mShaderProgram = Shader;

            // creamos nuestro indices
            mIndices = new ushort[6];
            mIndices[0] = 0;
            mIndices[1] = 1;
            mIndices[2] = 2;
            mIndices[3] = 0;
            mIndices[4] = 2;
            mIndices[5] = 3;

            // creamos nustros vertices
            mVertices = new float[10 * 4];
            // nuestro primer vertice
            mVertices[0] = 0f; mVertices[1] = 0f; mVertices[2] = 0f; mVertices[3] = 1f; // vertex
            mVertices[4] = 1f; mVertices[5] = 1f; mVertices[6] = 1f; mVertices[7] = 1f; // color
            mVertices[8] = 0f; mVertices[9] = 0f; // texture coordinate
            // nuestro segundo vertice
            mVertices[10] = this.Width; mVertices[11] = 0f; mVertices[12] = 0f; mVertices[13] = 1f; // vertex
            mVertices[14] = 1f; mVertices[15] = 1f; mVertices[16] = 1f; mVertices[17] = 1f; // color
            mVertices[18] = 1f; mVertices[19] = 0f; // texture coordinate
            // nuestro tercer vertice
            mVertices[20] = this.Width; mVertices[21] = this.Height; mVertices[22] = 0f; mVertices[23] = 1f; // vertex
            mVertices[24] = 1f; mVertices[25] = 1f; mVertices[26] = 1f; mVertices[27] = 1f; // color
            mVertices[28] = 1f; mVertices[29] = 1f; // texture coordinate
            // nuestro cuarto vertice
            mVertices[30] = 0f; mVertices[31] = this.Height; mVertices[32] = 0f; mVertices[33] = 1f; // vertex
            mVertices[34] = 1f; mVertices[35] = 1f; mVertices[36] = 1f; mVertices[37] = 1f; // color
            mVertices[38] = 0f; mVertices[39] = 1f; // texture coordinate
        }

        public Sprite2D(string path)
        {
            // cargamos nuestra textura
            mTexture2D = new NTexture2D(NativeApplication.instance.Device);
            mTexture2D.Load(path);

            // cargamos y construimos nuestro Shader
            mShaderProgram = new ShaderTextureProgram(NativeApplication.instance.Device);
            mShaderProgram.Load("Content/Fx_TexturePrimitive.fx");

            

            // creamos nuestro indices
            mIndices = new ushort[6];
            mIndices[0] = 0;
            mIndices[1] = 1;
            mIndices[2] = 2;
            mIndices[3] = 0;
            mIndices[4] = 2;
            mIndices[5] = 3;

            // creamos nustros vertices
            mVertices = new float[10 * 4];
            // nuestro primer vertice
            mVertices[0] = 0f; mVertices[1] = 0f; mVertices[2] = 0f; mVertices[3] = 1f; // vertex
            mVertices[4] = 1f; mVertices[5] = 1f; mVertices[6] = 1f; mVertices[7] = 1f; // color
            mVertices[8] = 0f; mVertices[9] = 0f; // texture coordinate
            // nuestro segundo vertice
            mVertices[10] = this.Width; mVertices[11] = 0f; mVertices[12] = 0f; mVertices[13] = 1f; // vertex
            mVertices[14] = 1f; mVertices[15] = 1f; mVertices[16] = 1f; mVertices[17] = 1f; // color
            mVertices[18] = 1f; mVertices[19] = 0f; // texture coordinate
            // nuestro tercer vertice
            mVertices[20] = this.Width; mVertices[21] = this.Height; mVertices[22] = 0f; mVertices[23] = 1f; // vertex
            mVertices[24] = 1f; mVertices[25] = 1f; mVertices[26] = 1f; mVertices[27] = 1f; // color
            mVertices[28] = 1f; mVertices[29] = 1f; // texture coordinate
            // nuestro cuarto vertice
            mVertices[30] = 0f; mVertices[31] = this.Height; mVertices[32] = 0f; mVertices[33] = 1f; // vertex
            mVertices[34] = 1f; mVertices[35] = 1f; mVertices[36] = 1f; mVertices[37] = 1f; // color
            mVertices[38] = 0f; mVertices[39] = 1f; // texture coordinate
        }

        public bool HitTest(Sprite2D other)
        {
            return ((this.X + this.Width * this.ScaleX) > other.X &&
                (this.Y + this.Height * this.ScaleY) > other.Y &&
                this.X < (other.X + other.Width * other.ScaleX) &&
                this.Y < (other.Y + other.Height * other.ScaleY));
        }

        public bool HitTest(int x, int y)
        {
            return 
                ((this.X + this.Width * this.ScaleX) > x && 
                (this.Y + this.Height * this.ScaleY) > y && 
                this.X < x && 
                this.Y < y);
        }

        public void UpdateDraw(float dt, RenderCamera camera)
        {
            mShaderProgram.Update(mVertices, mIndices);
            mShaderProgram.Draw(camera.transformed, mTexture2D);
        }
        public void Destroy()
        {
            mIndices = null;
            mVertices = null;

            mTexture2D.Destroy();
            mTexture2D = null;

            mShaderProgram.Destroy();
            mShaderProgram = null;
        }

        
    }
}
