﻿using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.shader.d3d;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOFramework
{
    public class StaticModel
    {
        #region PosRot

        public float X;
        public float Y;
        public float Z;

        public float RotationX;
        public float RotationY;
        public float RotationZ;

        public Matrix Rotation;

        #endregion

        private NModel _art;
        private Shader3DProgram _shader;

        public StaticModel(NModel Art, Shader3DProgram Shader)
        {
            _shader = Shader;
            _art = Art;
            _art.SetShader(_shader);
        }

        public void UpdateDraw(RenderCamera Camera, float dt)
        {
            _shader.X = X;
            _shader.Y = Y;
            _shader.Z = Z;

            _shader.mRotation = Rotation;

            _art.Draw(Camera.transformed, (int)dt);

        }
    }
}
