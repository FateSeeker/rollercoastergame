﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;

using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.utils;
using FOFramework.Hud;
using Sesion2_Lab01;
using Sesion2_Lab01.com.isil.data;

namespace FOFramework
{
    public class Game
    {
        private RenderCamera _camera;
        public RenderCamera RenderCamera { set { _camera = value; } get { return _camera; } }

        

        private Device _device;
        public Device Device { get { return _device; } }

        private NKeyboardHandler _keyboardHandler;
        private NMouseHandler _mouseHandler;

        public NKeyboardHandler Keyboard { get { return _keyboardHandler; } }
        public NMouseHandler Mouse { get { return _mouseHandler; } }

        public List<Scene> _scenes;

        private Scene _currentScene;

        private HUD _hud;

        public HUD HUD { set { _hud = value; } get { return _hud; } }

        public Scene CurrentScene
        {
            get { return _currentScene; } 
        }

        public Game(Device Device)
        {
            _device = Device;
            
        }

        public virtual void Initialize()
        {
            _keyboardHandler = new NKeyboardHandler(NativeApplication.instance.RenderForm, OnKeyDown, OnKeyUp);
            _mouseHandler = new NMouseHandler(NativeApplication.instance.RenderForm);
            
            _scenes = new List<Scene>();
        }

        public void AddScene(Scene NewScene)
        {
            _scenes.Add(NewScene);            
        }

        public void SetCurrentScene(int SceneIndex)
        {
            if(_currentScene != null)
            {
                _currentScene.Free();
                _currentScene = null;
            }
            
            _currentScene = _scenes[SceneIndex];
            _currentScene.Initialize();
        }

        public virtual void OnKeyDown(int Keycode)
        {
            _currentScene.OnKeyDown(Keycode);
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnKeyDown(Keycode);
            }
        }

        public virtual void OnKeyUp(int Keycode)
        {
            _currentScene.OnKeyUp(Keycode);
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnKeyUp(Keycode);
            }
        }

        public virtual void Update(float dt)
        {            
            
            if(Mouse.TouchCollection.Count > 0 )
            {
                switch (Mouse.TouchCollection.Last().TouchState)
                {
                    case NTouchLocationState.Pressed:
                        OnMouseDown(Mouse.TouchCollection.Last().Position);
                        break;
                    case NTouchLocationState.Moved:
                        OnMouseHover(Mouse.TouchCollection.Last().Position);
                        break;
                    case NTouchLocationState.Released:
                        
                        OnMouseUp(Mouse.TouchCollection.Last().Position);
                        break;
                }

            }

            _currentScene.Update(dt);

            if(_hud!=null)
            {
                if (_hud.HasContent()) _hud.Update(dt);
            }

            Mouse.Update((int)dt);
        }

        public void InitializeHUD()
        {

            _hud = new HUD(this);
            _hud.Initialize();
        }
        public void FreeHUD()
        {
            _hud.Free();
            _hud = null;
        }

        protected virtual void OnMouseDown(Vector2 MousePos)
        {
            _currentScene.OnMouseDown();
            if(_hud != null)
            {
                if (_hud.HasContent()) _hud.OnMouseDown(MousePos);
            }
        }

        protected virtual void OnMouseUp(Vector2 MousePos)
        {
            _currentScene.OnMouseUp();
            if (_hud != null)
            {
                if (_hud.HasContent()) _hud.OnMouseUp(MousePos);
            }
        }

        protected virtual void OnMouseHover(Vector2 MousePos)
        {
            if(_hud != null)
            {
                if (_hud.HasContent()) _hud.OnMouseHover(MousePos);
            }
        }

        public virtual void Draw(float dt)
        {
            NativeApplication.instance.EnableDepthTesting();
            _camera.ChangeCameraTo(RenderCamera.PERSPECTIVE);
            _currentScene.Draw(dt);
            if(_hud != null)
            {
                if(_hud.HasContent())
                {

                    _camera.ChangeCameraTo(RenderCamera.ORTHOGRAPHIC);

                    NativeApplication.instance.DisableDepthTesting();

                    _hud.Draw(dt);

                    

                }
            }
        }
    }
}
