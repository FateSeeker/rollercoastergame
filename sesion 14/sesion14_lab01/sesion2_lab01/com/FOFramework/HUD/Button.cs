﻿

using Sesion2_Lab01;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.shader.d2d;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOFramework.Hud
{
    public class Button:HUDObject
    {
        private Sprite2D _artOnClickDown;
        private Sprite2D _artOnClickUp;
        private Sprite2D _artOnHover;

        private Sprite2D _currentArt;

        private Action _OnClickDown;

        private Action _OnClickUp;

        private Action _OnMouseHover;

        

        public Button(HUD Parent,string ArtPath,ShaderTextureProgram Shader ,Vector2 Position):base(Parent)
        {
            SetOnClickUpArt(ArtPath,Shader);
            _currentArt = _artOnClickUp;
            _rect.Position = Position;
            _rect.Width = _currentArt.Width;
            _rect.Height = _currentArt.Height;
        }

        

        public override void Update(float dt)
        {
            _currentArt.X = Position.X;
            _currentArt.Y = Position.Y;
        }

        public void SetOnClickDown(Action OnClickDownAction)
        {
            if (OnClickDownAction != null) _OnClickDown = OnClickDownAction;
        }

        public void SetOnClickUp(Action OnClickUpAction)
        {
            if (OnClickUpAction != null) _OnClickUp = OnClickUpAction;
        }

        public void SetOnMouseHover(Action OnMouseHoverAction)
        {
            if (OnMouseHoverAction != null) _OnMouseHover = OnMouseHoverAction;
        }

        public void SetOnClickDownArt(string Path, ShaderTextureProgram Shader)
        {
            _artOnClickDown = new Sprite2D(Path, Shader);
        }
        public void SetOnClickUpArt(string Path,ShaderTextureProgram Shader)
        {
            _artOnClickUp = new Sprite2D(Path, Shader);
        }
        public void SetOnHoverArt(string Path, ShaderTextureProgram Shader)
        {
            _artOnHover = new Sprite2D(Path, Shader);
        }

        public override void OnMouseDown()
        {
            if (_artOnClickDown != null) _currentArt = _artOnClickDown;
            if (_OnClickDown != null) _OnClickDown();
        }

        public override void OnMouseUp()
        {
            if (_artOnClickUp != null) _currentArt = _artOnClickUp;
            if (_OnClickUp != null) _OnClickUp();

        }

        public override void OnMouseHover()
        {
            if (_artOnHover != null) _currentArt = _artOnHover;
            if (_OnMouseHover != null) _OnMouseHover();

        }

        public override void Draw(RenderCamera Camera, float dt)
        {
            _currentArt.UpdateDraw(dt,Camera);
        }

        
    }
}
