﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

public static class BasicMovementFunctions
{
    

    public static Vector3 Bezier(Vector3 start, Vector3 end, Vector3 t1, Vector3 t2,float parameter)
    {
        return (1 - parameter) * (1 - parameter) * (1 - parameter) * start + 3 * (1 - parameter) * (1 - parameter) * parameter * t1 + 3 * (1 - parameter) * parameter * parameter * t2 + parameter * parameter * parameter * end;
    }

    public static Vector3 QuadBezier(Vector3 a, Vector3 b, Vector3 c, float Parameter)
    {
        return (1 - Parameter) * (1 - Parameter) * a + 2 * (1 - Parameter) * Parameter * b + (Parameter * Parameter) * c;
    }
}

