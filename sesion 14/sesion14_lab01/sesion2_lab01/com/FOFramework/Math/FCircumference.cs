﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;


namespace FOFramework.Math
{
    public class FCircumference
    {
        private List<Vector3> _points;

        private Vector3 _normal;

        private int _resolution;

        private float _radius;

        private Vector3 _center;

        public Vector3 Normal
        {
            get { return _normal; }
        }


        public FCircumference(Vector3 Center, float Radius,int Resolution , Vector3 Normal)
        {
            _center = Center;
            _resolution = Resolution;
            _normal = Normal;
            _radius = Radius;
        }

        private Vector3 GetPerpendicular(Vector3 normal)
        {
            return Vector3.Cross(Vector3.Cross(Vector3.UnitY, normal), normal);
        }

        private void MakePoints()
        {
            Vector3 baseAxis = GetPerpendicular(_normal);
            float angle = 0f;
            for (int i = 0; i < _resolution; i++)
            {
                angle = (float)System.Math.PI * 2 / _resolution * i;

                Vector3 direction = Vector3.Transform(baseAxis, Quaternion.RotationAxis(_normal, angle));

                _points.Add(_center + direction * _radius);
            }
        }

        public Vector3[] GetPoints()
        {
            return _points.ToArray();
        }

        public static Vector3[] GetPointsBasedOn(Vector3 Center,float Radius,int Resolution, Vector3 Normal)
        {
            List<Vector3> Points = new List<Vector3>();
            Vector3 baseAxis = Vector3.Cross(Vector3.Cross(Vector3.UnitY, Normal), Normal);
            float angle = 0f;
            for (int i = 0; i < Resolution; i++)
            {
                angle = (float)System.Math.PI * 2 / Resolution * i;

                Vector3 direction = Vector3.Transform(baseAxis, Quaternion.RotationAxis(Normal, angle));

                Points.Add(Center + direction * Radius);
            }

            return Points.ToArray();
        }
    }
}
