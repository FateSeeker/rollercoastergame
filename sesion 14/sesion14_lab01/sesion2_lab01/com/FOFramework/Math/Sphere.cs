﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01.com.FOFramework.Math
{
    public struct Sphere
    {
        public float Radius;
        public Vector3 Position;

        public bool Intersects(Sphere other)
        {
            return (Vector3.Distance(Position, other.Position) <= (Radius + other.Radius));
        }
    }
}
