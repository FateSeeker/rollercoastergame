﻿using Sesion2_Lab01.com.isil.components;
using Sesion2_Lab01.com.isil.content;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.render.components;
using Sesion2_Lab01.com.isil.shader.d2d;
using Sesion2_Lab01.com.isil.system.screenManager;
using Sesion2_Lab01.com.isil.system.soundSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01.com.game.gui {

    public class GuiWaraScreen : Screen {

        private NTextField2D mTextField;

        private RenderCamera mRenderCamera;

        private NSoundInstance mSoundInstance;

        private SimpleButton mSimpleButton;

        public GuiWaraScreen() : base() {
            mRenderCamera = new RenderCamera(-5f);
        }

        public override void Initialize() {
            base.Initialize();

            mTextField = new NTextField2D("Content/font/kronika/kronika_16");
            mTextField.text = "It's me Wara!";

            mSoundInstance = new NSoundInstance(1);
            mSoundInstance.BindDataBuffer("Content/sound/music_play_button.wav");
            mSoundInstance.Play(null);

            mSimpleButton = new SimpleButton(200, 40, "Content/buttons/spButton.png");
            mSimpleButton.ScaleX = 0.1f;
            mSimpleButton.ScaleY = 0.1f;
            mSimpleButton.OnClickCallback(this.OnClick);
        }

        private void OnClick()
        {
            //NativeApplication.instance.ScreenManager.GotoScreen(typeof(GuiStartMenu));
        }

        public override void OnKeyDown(int keyCode)
        {
            base.OnKeyDown(keyCode);

            if (mRenderCamera != null)
            {
                mRenderCamera.OnKeyDown(keyCode);
            }
        }

        public override void OnKeyUp(int keyCode)
        {
            base.OnKeyUp(keyCode);

            if (mRenderCamera != null)
            {
                mRenderCamera.OnKeyUp(keyCode);
            }
        }

        public override void UpdateDraw(int dt) {
            base.UpdateDraw(dt);

            // actualizamos nuestra camara
            mRenderCamera.Update();

            mSoundInstance.Update(dt);

            mSimpleButton.UpdateDraw(mRenderCamera, dt);

            mRenderCamera.ChangeCameraTo(RenderCamera.ORTHOGRAPHIC);

            mTextField.UpdateAndDraw(mRenderCamera, dt);
        }
    }
}
