﻿using Core.Model;
using CrossXDK.com.digitalkancer.modules.moderlLoaders.assimp;
using Sesion2_Lab01.com.isil.components;
using Sesion2_Lab01.com.isil.content;
using Sesion2_Lab01.com.isil.data_type;
using Sesion2_Lab01.com.isil.graphics;
using Sesion2_Lab01.com.isil.render.batcher;
using Sesion2_Lab01.com.isil.render.camera;
using Sesion2_Lab01.com.isil.render.components;
using Sesion2_Lab01.com.isil.render.graphics;
using Sesion2_Lab01.com.isil.shader.d2d;
using Sesion2_Lab01.com.isil.shader.d3d;
using Sesion2_Lab01.com.isil.shader.skinnedModel;
using Sesion2_Lab01.com.isil.system.screenManager;
using Sesion2_Lab01.com.isil.system.soundSystem;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion2_Lab01.com.game.gui {

    public class GuiStartMenu : Screen {

        private SkinnedModelInstance mSkinnedModel;
        private Plane3D mPlane3D;

        private NSoundInstance mSoundInstance;

        private NTextField2D mTextField;

        private RenderCamera mRenderCamera;

        private NSpriteBatch mSpriteBatch;

        private SimpleButton mSimpleButton;

        private Sprite2D mSprite1;
        private Sprite2D mSprite2;

        /*##################*/
        private Sprite2D mHappy;
        private Sprite2D mSad;
        private Sprite2D mSerio;

        private NSkyBox3D mSkyBox;
        /*##################*/

        public GuiStartMenu() : base() {
            //mTimeCounter = 0;

            mRenderCamera = new RenderCamera(-5f);
        }

        public override void Initialize() {
            base.Initialize();

            // cargamos y construimos nuestro Shader
            mPlane3D = new Plane3D("Content/spMario.png", 0, 0, 0, 10f);

            mSkinnedModel = new SkinnedModelInstance("Content/magician/magician.X", "Content/magician/", true);
            mSkinnedModel.GotoAnimation("Run", true);

            mTextField = new NTextField2D("Content/font/kronika/kronika_16");
            mTextField.text = "Hola, soy una prueba en 3D =)";
            mTextField.X = 100;
            mTextField.Y = 100;
            mTextField.TextColor = NColor.Brown;

            mSoundInstance = new NSoundInstance(0);
            mSoundInstance.BindDataBuffer("Content/sound/music_play_button.wav");
            mSoundInstance.Play(() => {
                mSoundInstance = null;  
            });

            mSpriteBatch = new NSpriteBatch("Content/spMario.png");

            mSimpleButton = new SimpleButton(20, 160, "Content/buttons/spButton.png");
            mSimpleButton.ScaleX = 0.3f;
            mSimpleButton.ScaleY = 0.3f;
            mSimpleButton.OnClickCallback(this.OnClick);


            mSprite1 = new Sprite2D("Content/spMario.png");
            mSprite1.X = 500;

            mSprite2 = new Sprite2D("Content/spMario.png");
            mSprite2.X = 600;

            mSkyBox = new NSkyBox3D("Content/skybox/spSkybox.png", 4, 3, 50000);
            mSkyBox.X = 0;
            mSkyBox.Y = 0;
            mSkyBox.Z = 0;

            mHappy = new Sprite2D("Content/happy.png");
            mHappy.X = 200;
            mHappy.Y = 400;
            mSerio = new Sprite2D("Content/serio.png");
            mSerio.X = 350;
            mSerio.Y = 400;
            mSad = new Sprite2D("Content/sad.png");
            mSad.X = 450;
            mSad.Y = 400;

            
        }

        private void OnClick()
        {
            //NativeApplication.instance.ScreenManager.GotoScreen(typeof(GuiWaraScreen));
        }

        public override void OnKeyDown(int keyCode)
        {
            base.OnKeyDown(keyCode);

            if (mRenderCamera != null)
            {
                mRenderCamera.OnKeyDown(keyCode);
            }
        }

        public override void OnKeyUp(int keyCode)
        {
            base.OnKeyUp(keyCode);

            if (mRenderCamera != null)
            {
                mRenderCamera.OnKeyUp(keyCode);
            }
        }

        public override void UpdateDraw(int dt) {
            base.UpdateDraw(dt);

            // actualizamos nuestra camara
            mRenderCamera.Update();

            if (mSoundInstance != null)
            {
                mSoundInstance.Update(dt);
            }


            NativeApplication.instance.EnableDepthTesting();

            // PRIMERO SE DIBUJA TODO LLO QUE TIENE QUE VER CON DEPTH BUFFERS OSEA 3D
            mRenderCamera.ChangeCameraTo(RenderCamera.PERSPECTIVE);
            mSkyBox.UpdateDraw(mRenderCamera, dt);
            mSkinnedModel.UpdateDraw(mRenderCamera, dt); 
            mPlane3D.UpdateAndDraw(mRenderCamera, dt);
            
            // FINALMENTE SE DIBUJA TODO LO QUE TIENE QUE VER CON 2D
            mRenderCamera.ChangeCameraTo(RenderCamera.ORTHOGRAPHIC);

            NativeApplication.instance.DisableDepthTesting();

            mSprite1.UpdateDraw(dt, mRenderCamera);
            mSprite2.UpdateDraw(dt, mRenderCamera);
            mHappy.UpdateDraw(dt, mRenderCamera);
            mSerio.UpdateDraw(dt, mRenderCamera);
            mSad.UpdateDraw(dt, mRenderCamera);
            
            //mSimpleButton.X += 1;
            mSimpleButton.UpdateDraw(mRenderCamera, dt);

            // sprite batch
            mSpriteBatch.AddSprite(50, 50, 0, 60, 60, NColor.White);
            mSpriteBatch.AddSprite(150, 0, 0, 120, 120, NColor.Blue);
            mSpriteBatch.UpdateAndDraw(mRenderCamera, dt);

            // text
            mTextField.UpdateAndDraw(mRenderCamera, dt);
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
